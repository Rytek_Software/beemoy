#ifndef BEEMOYCOMMON_H
#define BEEMOYCOMMON_H

/*
 * This code is not for commercial use until otherwise specified.
 */

/*
 * This code, and application makes use of SDL 2.0 libraries, they fall under the zlib license,
 * my code on the other hand does not.
 * I do NOT claim to own SDL or it's libraries, I did not modify its composition or code, nor
 * do I wish to misrepresent them.
 */

// Macro definition used to prevent overhead incurred by function calls
#define HOST_L_ENDIAN 1
#define FLIP_ENDIAN(sh) if(HOST_L_ENDIAN){((unsigned char *)(sh))[0] = ((unsigned char *)(sh))[0] ^ ((unsigned char *)(sh))[1]; ((unsigned char *)(sh))[1] = ((unsigned char *)(sh))[0] ^ ((unsigned char *)(sh))[1]; ((unsigned char *)(sh))[0] = ((unsigned char *)(sh))[0] ^ ((unsigned char *)(sh))[1];}

// Success and Failure Signals
#define FINISH_SUCCESS 0
#define FINISH_FAILURE 1

// Single Clock Cycle Time in Nano Seconds, given ~4.19 MHz Clock Rate
#define CYCLE_TIME 238.418579

// Clock Cycles Required before refresh
#define REFRESH_WAIT_CYCLES_REAL 69905

// Corrected for lack of temporal granularity
#define REFRESH_WAIT_CYCLES 70028

// Name of Game Address
#define GAMENAME_OFFSET 308

// The maximum memory address allowed by the GameBoy
const unsigned short MAX_ADDR = 0xFFFE;

// DEBUGGING THINGS
#define HALT_ON_UNKNOWN 1

#define JUMP_DEBUG 1

#define PRINTPC 0

#define VERBOSE_NOP 0
#define RUN_ONE_INSTR 0
#define VERBOSE_INSTR_NUM 0
#define VERBOSE_REGISTERS 0

// Convert a Register value to a cout compatible hex value
#define REG2HX(reg)   ( (((*reg) >> 4) > (unsigned char)(9)) ? (char)(65 + (((*reg) >> 4)-10)) : (char)((*reg) >> 4) )<< ( (((*reg) & 0x0F) > (unsigned char)(9)) ? (char)(65 + (((*reg) & 0x0F)-10)) : (char)((*reg) & 0x0F) ) <<       ( (((reg[1]) >> 4) > (unsigned char)(9)) ? (char)(65 + (((reg[1]) >> 4)-10)) : (char)((reg[1]) >> 4) )<< ( (((reg[1]) & 0x0F) > (unsigned char)(9)) ? (char)(65 + (((reg[1]) & 0x0F)-10)) : (char)((reg[1]) & 0x0F) )

#define DEBUG_COUTREG if(VERBOSE_REGISTERS){cout << "AF: " << REG2HX(af)<< "; BC: "<< REG2HX(bc)<< "; DE: "<< REG2HX(de)<< "; HL: "<< REG2HX(hl)<< "; SP: "<< REG2HX(tsp)<< "; PC: "<< REG2HX(tpc)<< endl;}

// Makes CPU Timing accurate to ~4.19Mhz like a real GB Z80, this will cause delays and real lag if render speed is not fast enough
#define ACCURATE_CPU_TIMING 0

// Register Flag Help
#define ZERO 128
#define SUBTRACT 64
#define HALFCARRY 32
#define CARRY_CP 16

#endif /* BEEMOYCOMMON_H */
//
