// By: Ryan Abouchakra

#ifndef GBZ80_H
#define GBZ80_H

#include "beemoy_common.h"
#include "memoryUnit.h"
#include <iostream>

// C++ 11 Library
#include <chrono>

using namespace std;
using namespace std::chrono;

/****************************************/
/* The CPU is the GameBoy's take on on the Zilog's Z80 which is related to
 * Intel's 8080
 */
class GBZ80

{
private:

    // CPU Registers
    unsigned char *af, *bc, *de, *hl;
    unsigned short *sp, *pc;

    // Generic Temp byte used for transfers
    unsigned char *tmp;

    // Memory Unit
    memoryUnit * mem_unit;

    // Initial PC and SP
    #define INITIAL_PC (unsigned short)0x0100
    #define INITIAL_SP (unsigned short)0xFFFE

    // Running Flag
    int runnable;
    int interrupts_enabled;

public:

    // Constructor
    GBZ80(char * rom_fn);

    // Check if PC is valid
    int checkPCValid();

    unsigned short toShort(unsigned char * char_array);

    // Begins or Continues executing CPU instructions
    void run();

    // Resume CPU state same as run, here for convenience
    void resume();

    // Freezes the CPU state
    void pause();

    // Write Char to Register
    void writeToReg(unsigned char value, unsigned char * reg);

    // Write Short to Register
    void writeToReg(unsigned short value, unsigned char * reg);

    // Increment PC
    int updatePC();

    // Change the value of PC
    int updatePC(unsigned short new_pc);

    // Change the value of PC
    int updateSP(unsigned short new_sp);

    // Return the pointer to the memoryUnit
    memoryUnit * getMemUnit();

};
/****************************************/

/****************************************
 * Constructor
 ****************************************/
GBZ80::GBZ80(char * rom_fn) {

    cout << "Instantiating CPU!" << endl;

    // Creating new registers
    af = new unsigned char[2];
    bc = new unsigned char[2];
    de = new unsigned char[2];
    hl = new unsigned char[2];

    // Generic tmp byte used for transfers
    tmp = new unsigned char;

    // Initiating SP and PC registers
    sp = new unsigned short;
    pc = new unsigned short;
    updateSP(INITIAL_SP);
    updatePC(INITIAL_PC);

    // Initiating Memory
    mem_unit = new memoryUnit(rom_fn);

    // Set the CPU Running Status O
    runnable = 1;

    // Set Interrupts Enabled
    interrupts_enabled = 1;

}
/****************************************/

/****************************************
 * Check if PC is valid
 ****************************************/
int GBZ80::checkPCValid() {
    if (*pc > MAX_ADDR) {
        cout << "End of Memory!" << endl;
        return 0;
    } else {
        return 1;
    }
}
/****************************************/

unsigned short GBZ80::toShort(unsigned char * char_array) {
    unsigned short t;
    ((unsigned char*) (&t))[0] = char_array[1];
    ((unsigned char*) (&t))[1] = char_array[0];

    return t;

}

/****************************************
 * Begins or Continuous executing CPU
 * instructions.
 *
 * If cpu is paused it will not Run again
 * until Resumed.
 ****************************************/
void GBZ80::run() {

    // The number of cycles the instruction requires
    int cycles = 0;
    int cycles_since_refresh = 0;

    // Notify that the CPU is now running
    cout << "CPU is running..." << endl;

    // Used to get the exact time of the start of a Instruction
    steady_clock::time_point start_time;

    // For DEBUG_COUTREG Verbosity
    unsigned char * tsp = (unsigned char *) sp;
    unsigned char * tpc = (unsigned char *) pc;


    // Main CPU Loop
    while (runnable) {

        // Note the start time of the instruction in nanoseconds
        start_time = steady_clock::now();

        // Debug statement to print the PC
        if (PRINTPC)
            cout << "PC: " << (int) (*pc) << endl;

        // Fetch, Decode and Execute Instruction
        switch (mem_unit->readChar(*pc)) {

        /* General --------------------------------------------------------------- */

        // NOP
        case 0x00:
            cycles = 4;
            break;

            // Jump Immediate
        case 0xC3:
            cycles = 12;
            *pc = ((mem_unit->readShort(++(*pc))) - 1); // Minus 1 to compensate for updatePC later
            if (JUMP_DEBUG)
                cout << "Jumped to: " << (int) ((*pc) + 1) << endl;
            break;

            // Enable Interrupts
        case 0xFB:
            cycles = 4;
            interrupts_enabled = 1;
            break;

            // Disable Interrupts
        case 0xF3:
            cycles = 4;
            interrupts_enabled = 0;
            break;


        /* RLCA (Rotate Left Carry on A) --------------------------------------- */

        case 0x07:
            cycles = 4;
            *tmp = af[0]; // Make a Copy
            af[0] <<= 1;  // Shift left 1 bit
            af[0] += ((*tmp & 0x80) >> 7); // Take the old leftmost bit and copy to the right bit.
            af[1] &= (~CARRY_CP); // Erase the Carry Bit value
            af[1] += ((*tmp & 0x80) >> 3); // Update the Carry flag to be the old bit.
            break;



        /* Calls --------------------------------------------------------------- */

            // Place next instruction onto the stack then jump to the address parameter
        case 0xCD:
            cycles = 12;

            // Make Room on the Stack
            (*sp) -= 2;

            // Copy PC to stack
            mem_unit->writeToMem((unsigned short) (++(*pc)), *sp);

            // Jump to Corresponding Addr
            *pc = (mem_unit->readShort(*pc) - 1); // Minus 1 to compensate for updatePC later
            if (JUMP_DEBUG)
                cout << "Call Jumped to: " << (int) ((*pc) + 1) << endl;

            break;

        /* Returns --------------------------------------------------------------- */

        // Return if Carry is zero
        case 0xD0:
            cycles = 8;

            if((af[1] & CARRY_CP) == 0){
                // Copy from stack to PC
                *pc = (mem_unit->readShort(*sp)); // No need to compensate for updatePC as we want to go to the next instruction
                if (JUMP_DEBUG) cout << "Return Jumped to: " << (int) ((*pc) + 1) << endl;

                // Move SP Back
                (*sp) += 2;
            }

            break;


        // Return and Enable Interrupts
        case 0xD9:
            cycles = 8;

            // Copy from stack to PC
            *pc = (mem_unit->readShort(*sp)); // No need to compensate for updatePC as we want to go to the next instruction
            if (JUMP_DEBUG) cout << "Return Jumped to: " << (int) ((*pc) + 1) << endl;

            // Move SP Back
            (*sp) += 2;

            interrupts_enabled = 1;

            break;

            /* Restarts --------------------------------------------------------------- */

            // With $38 Jump
        case 0xFF:
            cycles = 32;

            // Make Room on the Stack
            (*sp) -= 2;

            // Copy PC to stack
            mem_unit->writeToMem((unsigned short) (*pc), *sp);

            // Jump to Corresponding Addr
            *pc = (unsigned short) (0x0038 - 1); // Minus 1 to compensate for updatePC later
            if (JUMP_DEBUG)
                cout << "Restart Jumped to: " << (int) ((*pc) + 1) << endl;

            break;

            /* Load High ---------------------------------------------------------- */

            // Load from A into memory address with offset from $FF00
        case 0xE0:
            cycles = 12;
            mem_unit->writeToMem(af[0], (mem_unit->readChar(++(*pc)) + 0xFF00));
            break;

            // Load from memory address with offset from $FF00 into A
        case 0xF0:
            cycles = 12;
            af[0] = mem_unit->readChar(mem_unit->readChar(++(*pc)) + 0xFF00);
            break;

            /* 8 bit load ---------------------------------------------------------- */

            // Load 8 bit immediate value
        case 0x3E:
            cycles = 8;
            af[0] = mem_unit->readChar(++(*pc));
            break;

            /* 16 bit load ---------------------------------------------------------- */

            // Load 16 bit value to BC
        case 0x01:
            cycles = 12;
            bc[1] = mem_unit->readChar(++(*pc));
            bc[0] = mem_unit->readChar(++(*pc));
            break;

            // Load 16 bit value to DE
        case 0x11:
            cycles = 12;
            de[1] = mem_unit->readChar(++(*pc));
            de[0] = mem_unit->readChar(++(*pc));
            break;

            // Load 16 bit value to HL
        case 0x21:
            cycles = 12;
            hl[1] = mem_unit->readChar(++(*pc));
            hl[0] = mem_unit->readChar(++(*pc));
            break;

            // Load 16 bit value to SP
        case 0x31:
            cycles = 12;
            *sp = mem_unit->readShort(++(*pc));
            break;

            /* 2 Byte Ops ---------------------------------------------------------- */

        case 0xCB:
            switch (mem_unit->readChar(++(*pc))) {

            /* SET ---------------------------------------------------------- */

            // Set Bit on A register to 1
            case 0xC7:
                cycles = 8;
                af[0] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on B register to 1
            case 0xC0:
                cycles = 8;
                bc[0] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on C register to 1
            case 0xC1:
                cycles = 8;
                bc[1] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on D register to 1
            case 0xC2:
                cycles = 8;
                de[0] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on E register to 1
            case 0xC3:
                cycles = 8;
                de[1] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on H register to 1
            case 0xC4:
                cycles = 8;
                hl[0] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on L register to 1
            case 0xC5:
                cycles = 8;
                hl[1] |= (1 << (mem_unit->readChar(++(*pc)) - 1));
                break;

                // Set Bit on HL register to 1
            case 0xC6:
                cycles = 16;
                mem_unit->writeToMem(
                        ((unsigned char) (mem_unit->readChar(toShort(hl))
                                | (1 << (mem_unit->readChar(++(*pc)) - 1)))),
                        toShort(hl));
                break;
                /* ----------------------------------------------------------------- */

            default:
                cout << "UNKNOWN 2 Byte INSTRUCTION: (" << "CB "
                        << ((unsigned int) mem_unit->readChar(*pc))
                        << ") AT LOCATION: (" << ((unsigned int) *pc) << ")"
                        << endl;
                if (HALT_ON_UNKNOWN)
                    pause();
                break;
            }
            /* ----------------------------------------------------------------- */

        default:
            cout << "UNKNOWN INSTRUCTION: ("
                    << ((unsigned int) mem_unit->readChar(*pc))
                    << ") AT LOCATION: (" << ((unsigned int) *pc) << ")"
                    << endl;
            if (HALT_ON_UNKNOWN)
                pause();
            break;
        }

        // Increment the program counter if end of memory reached end execution.
        if (runnable)
            runnable = updatePC();

        // Cout all registers per instruction, used for DEBUG
        DEBUG_COUTREG

        // Wait the appropriate cycle * (cycle time), if necessary
        if (ACCURATE_CPU_TIMING) {
            while (duration_cast<nanoseconds>(steady_clock::now() - start_time).count()
                    < (CYCLE_TIME * cycles))
                ;
        }

        cycles_since_refresh += cycles;
        if (cycles_since_refresh >= REFRESH_WAIT_CYCLES) {
            cycles_since_refresh = 0;

            // TODO Render GPU
            cout << "GPU Render" << endl;

            while (duration_cast<nanoseconds>(steady_clock::now() - start_time).count()
                    < (CYCLE_TIME * REFRESH_WAIT_CYCLES))
                ;
        }
        cycles = 0;

    }
    cout << "Execution Halted!" << endl;
}
/****************************************/

/****************************************
 * Resumes CPU state
 ****************************************/
void GBZ80::resume() {
    if (checkPCValid()) {
        run();
    }
}
/****************************************/

/****************************************
 * Freezes the CPU state
 ****************************************/
void GBZ80::pause() {
    runnable = 0;
}
/****************************************/

/****************************************
 * Write Char to Register
 *****************************************/
void GBZ80::writeToReg(unsigned char value, unsigned char * reg) {
    *reg = value;
}
/****************************************/

/****************************************
 * Write Short to Register
 ****************************************/
void GBZ80::writeToReg(unsigned short value, unsigned char * reg) {
    reg[0] = (value & 0xFF);
    reg[1] = ((value >> 8) & 0xFF);
}
/****************************************/

/****************************************
 * Increment PC
 ****************************************/
int GBZ80::updatePC() {
    (*pc)++;
    return checkPCValid();
}
/****************************************/

/****************************************
 * Change the value of PC
 ****************************************/
int GBZ80::updatePC(unsigned short new_pc) {
    *pc = new_pc;
    return checkPCValid();
}
/****************************************/

/****************************************
 * Change the value of PC
 ****************************************/
int GBZ80::updateSP(unsigned short new_sp) {
    *sp = new_sp;
    return checkPCValid();
}
/****************************************/

/****************************************
 * Returns the pointer to the memory unit
 ****************************************/
memoryUnit * GBZ80::getMemUnit() {
    return mem_unit;
}
/****************************************/

// Comment formatting
/****************************************
 *
 ****************************************/

/****************************************/

#endif
//
