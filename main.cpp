/* I legally own a physical copy of every rom backup I test */

#include "beemoy_common.h"
#include <iostream>
#include "GBZ80.h"

using namespace std;

int main(int argc, char * argv[]) {

    char * rom_filename;

    // Argument Passed in
    if (argc >= 2) {
        rom_filename = argv[2];
    } else {
        cout << "File Name Unspecified Defaulting to Hello World..." << endl;
        rom_filename = (char *) ("roms/hw.gb");
    }

    cout << "Hello Beemoy!" << endl;

    // Create a Gameboy Z80 cpu and load its memory with a rom
    GBZ80 * gbcpu = new GBZ80(rom_filename);

    // Run the Gameboy
    gbcpu->run();

    cout << "Good Bye Beemoy!" << endl;
    return 0;
}
//
