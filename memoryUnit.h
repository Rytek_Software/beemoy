#ifndef MEMORYUNIT_H
#define MEMORYUNIT_H
#include "beemoy_common.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

/* Z80 Memory is stored as Little Endian (This emulator is designed to run
 * on a Little Endian System Also)
 *
 *  The real GB mmu would map the address directly to the real cart, where as this
 *  one copies the entire ROM into RAM for the purpose of simplicity.
 */
class memoryUnit {

private:
    // Main Memory Array
    unsigned char * memory;

    // Game Name
    unsigned char * game_name;

public:

    // Constructor
    memoryUnit(char * rom_fn);

    // Loads 32k Rom into Memory
    void romLoader(char * rom_fn);

    // Check if the given memory location is valid
    int checkLocationValid(int location);

    // Write Char to memory
    int writeToMem(unsigned char value, int location);

    // Write Short to memory
    int writeToMem(unsigned short value, int location);

    // Read Char
    unsigned char readChar(int location);

    // Read Short
    unsigned short readShort(int location);

};

/****************************************
 * Loads 32k Rom into Memory
 ****************************************/
void memoryUnit::romLoader(char * rom_fn) {

    FILE *rom_fptr;
    long rom_flen = 0;

    // Read rom file as a binary
    rom_fptr = fopen(rom_fn, "rb");

    if (rom_fptr == NULL) {
        cout << "Cannot Find a Rom File named: " << rom_fn << endl;
        exit(EXIT_FAILURE);
    } else {
        cout << "Loaded Rom File: " << rom_fn << endl;

    }

    // Get Length by finding the end of the file and get its byte position
    fseek(rom_fptr, 0, SEEK_END);
    rom_flen = ftell(rom_fptr);

    // Return to the beginning of the file
    rewind(rom_fptr);

    cout << "ROM SIZE: " << (int) rom_flen << endl;

    // Copy the entire rom into memory
    fread(memory, 1, rom_flen, rom_fptr);

    // Close the rom file
    fclose(rom_fptr);

}
/****************************************/

/****************************************
 * Constructor
 ****************************************/
memoryUnit::memoryUnit(char * rom_fn) {

    // Create the RAM
    memory = new unsigned char[MAX_ADDR + 1];

    // Zero fill the RAM
    for (int i = 0; i <= MAX_ADDR; i++) {
        memory[i] = (unsigned char) 0x00;
    }

    // Load 32K ROM into RAM
    romLoader(rom_fn);

    // Print Game Name
    game_name = ((unsigned char *) (&(memory[GAMENAME_OFFSET])));
    cout << "Game Name: " << game_name << endl;
}
/****************************************/

/****************************************
 * Check if PC is valid
 ****************************************/
int memoryUnit::checkLocationValid(int location) {
    if (location > MAX_ADDR) {
        return 0;
    } else {
        return 1;
    }
}
/****************************************/

/****************************************
 * Write Char to memory
 *****************************************/
int memoryUnit::writeToMem(unsigned char value, int location) {

    if (checkLocationValid(location)) {
        memory[location] = value;
        return 1;
    } else {

        cout << "Cannot Write Char Out of Bounds" << endl;
        return 0;
    }
}
/****************************************/

/****************************************
 * Write Short to memory
 ****************************************/
int memoryUnit::writeToMem(unsigned short value, int location) {

    if (checkLocationValid(location + 1)) {
        memory[location] = ((unsigned char *) (&value))[0];
        memory[location + 1] = ((unsigned char *) (&value))[1];

        return 1;
    } else {
        cout << "Cannot Write Short Out of Bounds" << endl;
        return 0;
    }
}
/****************************************/

/****************************************
 * Read Char
 ****************************************/
unsigned char memoryUnit::readChar(int location) {
    if (checkLocationValid(location)) {
        return memory[location];
    } else {
        cout << "Cannot Read Char Out of Bounds" << endl;
        return 0;
    }
}
/****************************************/

/****************************************
 * Read Short used for SP and PC
 ****************************************/
unsigned short memoryUnit::readShort(int location) {
    if (checkLocationValid(location + 1)) {
        unsigned short ts;
        ((unsigned char *) (&ts))[0] = memory[location];
        ((unsigned char *) (&ts))[1] = memory[location + 1];
        return ts;
    } else {
        cout << "Cannot Read Short Out of Bounds" << endl;
        return 0;
    }
}
/****************************************/

// Comment formatting
/****************************************
 *
 ****************************************/

/****************************************/

#endif
//
